============================
Issuing Digital Certificates
============================

Issue Introduction
------------------

As the name suggests, A certificate should be sent to the user through digital media as a token of completion of the course. Scholars, you can think "We already have that feature in YAKSH Platform" but this is different from that. We have jotted it down. You people get it, once if you skim through this.

Work Flow:
----------
	.. image:: images/workflow.png


Why we have chosen this issue?
------------------------------
These days peeps are expecting the result even without completing their work. But we are not getting the result at the time and as googly we are asked to refill the details but we lazy programmers don't want to do it. We decided to fix this issue, We will be issuing a certificate so that the user can get the certificate the very next minute of the completion of the course. So to send the certificate we may have various ways for all those ways we need to get extra data or information but to make it user friendly or student-friendly we are not taking any bit of extra data. But the people who are reading this, a doubt might have already raised in your mind that how can the user get their certificate. Here is your answer we will be sending the certificate through the mail which the user gives to register into our platform.

How we fixed it??
-----------------

   **1.Certifcate Generation**
	We have decided to generate the certificate using LaTeX, we can generate it through HTML but to transcend we have used LaTeX.Before embarking into it, the initial step is the LaTeX installation.

	**Command to install LaTeX:**
	   sudo apt-get install texlive
	
	**Certificate**

       Following is the sample Certificate generated using Latex.

          .. image:: images/certificate.png
		   :scale: 30



   **2.Integrating certificate with code**
	 As the project is totally related to Django and Python the next step is all about how we integrated our TeX file(which we have used to generate certificate) and Python file. As every certificate contains the user name and course name to render those we have sent them as arguments using the Jinja template engine. We have used the jinja2 module to achieve this.And then we have coverted this file to pdf format using pdflatex.

	**Command to install Jinja2:**::
	   pip install jinja2

	**Command to install pdflatex**::
	   pip install pdflatex

   **3.Fetching Data from Yaksh Database:**
	 The jinja2 module is just a means of transport to reach the destination but the source is Yaksh database. Course id and user id are taken from **yaksh_coursestatus** and their corresponding username and course name are fetched from **auth_user** and **yaksh_course** tables.

   **4.Sending Certificate via Mail:** 
	Once the user is done with the entire course, a certificate has to be sent to his/her mail. And after sending a certificate to the user, the information is updated in the Certificate column in the Course_status table.

	**Sending Email**

			Email ids of users are fetched from auth_user using user_id.Finally, An email is sent to user's mail with a message of course completion and an attachment of a certificate

	
Code Modifications
------------------
**1.In online_test/online_test/settings.py/**
	In this file we have changed the values of variables as:
	 | 
	 | EMAIL_USE_TLS = True
	 | EMAIL_HOST = 'smtp.gmail.com'
	 | EMAIL_PORT = '587'
	 | EMAIL_HOST_USER = 'yakshcertificate@gmail.com'
         | EMAIL_HOST_PASSWORD = 'elite2019'
	 | EMAIL_USE_SSL = False

**2. In online_test/yaksh/send_emails.py/**
	Inorder to send mail to the user, in this file we mentioned the content of the body of email, subject and attachment(of Certificate) which has to be sent.

**3. In online_test/yaksh/models.py/**
	In this file in the class CourseStatus, we have added a single line of code inorder to add a column to the Course_status table in YAKSH Database.
        | And in the same class we have created two functions named get_certificateStatus() and set_certificateStatus() to know if the user received his/her certificate or not.

**4.In online_test/yaksh/views.py/**
	Here, we imported mail_certificate from send_emails. 
        | In Views Module, we have included a piece of code to generate a Certificate with his/her details upon Completion of a Course.


**5.In online_test/yaksh/templates/yaksh/course_modules.html**
	In this HTML file, we have included code to provide a button to get Certificate.
	


